macro(ADD_XILINX_BD bd_name)

set(BD_SRC_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${bd_name})
set(BD_BUILD_DIR ${CMAKE_CURRENT_BINARY_DIR}/${bd_name})

file(MAKE_DIRECTORY ${BD_BUILD_DIR})
if(NOT IS_DIRECTORY ${BD_SRC_DIRECTORY})
  message("Creating Block Design ${bd_name}...")
  execute_process(
    COMMAND vivado -mode batch -nojournal -nolog -source ${CMAKE_SOURCE_DIR}/scripts/create_bd.tcl -tclargs "${bd_name}"
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
  )
endif()

# Need to make a build folder for xilinx_ip and symlink files to build dir
foreach(file ".bd"  ".bmm"  ".bxml"  "_ooc.xdc")
  link_file("${BD_SRC_DIRECTORY}/${bd_name}${file}" "${BD_BUILD_DIR}/${bd_name}${file}")
  SET(BD_FILES ${BD_FILES} ${BD_SRC_DIRECTORY}/${bd_name}${file})
endforeach()

# need to run generate_bd if bd already existed to recreate xci files in build folder
add_custom_target(generate_bd
  vivado -mode batch -source ${CMAKE_SOURCE_DIR}/scripts/build_bd.tcl -tclargs "${BD_BUILD_DIR}/${bd_name}.bd"
  OUTPUT ${BD_BUILD_DIR}/hdl/xilinx_ip_wrapper.v
  WORKING_DIRECTORY ${BD_BUILD_DIR}
  COMMENT "Generating ${bd_name}"
)


add_custom_target(gui
  vivado -mode batch -source ${CMAKE_SOURCE_DIR}/scripts/open_bd.tcl -tclargs "${BD_BUILD_DIR}/${bd_name}.bd"
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
  COMMENT "Opening Vivado GUI"
  # need to run generate_bd after running gui
)

add_custom_target(elaborate
  vivado -mode batch -source ${CMAKE_SOURCE_DIR}/scripts/elaborate_bd.tcl -tclargs "${BD_BUILD_DIR}/${bd_name}.bd"
  WORKING_DIRECTORY ${BD_BUILD_DIR}
  COMMENT "Elaborating design and opening gui"
)

add_custom_target(build_ipi
  vivado -mode batch -source ${CMAKE_SOURCE_DIR}/scripts/build_ipi_ooc.tcl -tclargs "${BD_BUILD_DIR}/${bd_name}.bd" "${CMAKE_BINARY_DIR}/ipi_cache"
  WORKING_DIRECTORY ${BD_BUILD_DIR}
  COMMENT "Building IPI"
)

endmacro()

macro(ADD_XILINX_SIM)
add_custom_target(sim_libs
  vivado -mode batch -source ${CMAKE_SOURCE_DIR}/scripts/compile_simlibs.tcl -tclargs "${CMAKE_BINARY_DIR}/compile_simlib"
  WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
  COMMENT "Compiling Xilinx simlibs"
)
endmacro()
