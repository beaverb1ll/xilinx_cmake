macro(link_file source_file destination_file)
  set(FILE_COPY_COMMAND create_symlink)
  execute_process(COMMAND ${CMAKE_COMMAND} -E ${FILE_COPY_COMMAND} ${source_file} ${destination_file})
endmacro(link_file)
