set SCRIPT_DIR [file dirname $argv0]
set BD_FILE_NAME [lindex $argv 0]

source $SCRIPT_DIR/part_and_board.tcl

read_bd $BD_FILE_NAME

set_property ip_output_repo /home/user/git/vivado_scripted/build/ipi_cache [current_project]
set_property ip_cache_permissions {read} [current_project]

synth_design -top xilinx_ip -rtl
start_gui
