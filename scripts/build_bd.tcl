
set SCRIPT_DIR [file dirname $argv0]
set BD_FILE_NAME [lindex $argv 0]

source $SCRIPT_DIR/part_and_board.tcl

read_bd $BD_FILE_NAME
set generated_files [make_wrapper -files [get_files $BD_FILE_NAME] -fileset [get_filesets sources_1] -inst_template]
add_files -norecurse $generated_files

# Note: implementation must be built first,
# so it must be in front of list
set targets_to_build [list "implementation" "synthesis"]
set supported_targets [list_targets [get_files $BD_FILE_NAME]]

foreach target $targets_to_build {
  set is_supported [lsearch -exact $supported_targets $target]
  if {$is_supported != -1} {
    generate_target $target [get_files $BD_FILE_NAME]
  }
}


set_property ip_output_repo /home/user/git/vivado_scripted/build/ipi_cache [current_project]
set_property ip_cache_permissions {read} [current_project]

# synth_design -top xilinx_ip -rtl
# synth_design -top xilinx_ip
# write_checkpoint -force post_synth
#
# opt_design
# place_design
# phys_opt_design
# write_checkpoint -force post_place
#
# route_design
# write_checkpoint -force post_route
#
# write_xdc -no_fixed_only -force impl.xdc
#
# write_bitstream -force xilinx_ip.bit
# write_sysdef -hwdef ./synth/xilinx_ip.hwdef -bitfile xilinx_ip.bit -file xilinx_ip.hdf -force
