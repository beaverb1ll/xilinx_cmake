# The intent of this script is to build a single IP
# specified by the XCI file path and an output cache
# location.


set SCRIPT_DIR [file dirname $argv0]
set XCI_PATH [lindex $argv 0]
set CACHE_DIR [lindex $argv 1]


source $SCRIPT_DIR/part_and_board.tcl



proc create_report { reportName command } {
  set status "."
  append status $reportName ".fail"
  if { [file exists $status] } {
    eval file delete [glob $status]
  }
  send_msg_id runtcl-4 info "Executing : $command"
  set retval [eval catch { $command } msg]
  if { $retval != 0 } {
    set fp [open $status w]
    close $fp
    send_msg_id runtcl-5 warning "$msg"
  }
}



set_param project.vivado.isBlockSynthRun true
#set_msg_config -msgmgr_mode ooc_run

set_param project.singleFileAddWarning.threshold 0
set_param project.compositeFile.enableAutoGeneration 0
set_param synth.vivado.isSynthRun true
set_msg_config -source 4 -id {IP_Flow 19-2162} -severity warning -new_severity info
set_property XPM_LIBRARIES {XPM_CDC XPM_FIFO XPM_MEMORY} [current_project]
set_property default_lib xil_defaultlib [current_project]
set_property target_language Verilog [current_project]
set_property ip_output_repo $CACHE_DIR [current_project]
set_property ip_cache_permissions {read write} [current_project]

read_ip $XCI_PATH

foreach xdc_file [get_files -of_objects [get_files $XCI_PATH] -filter {FILE_TYPE == XDC}] {
  set_property used_in_implementation false [get_files -all $xdc_file]
}


foreach dcp [get_files -quiet -all -filter file_type=="Design\ Checkpoint"] {
  set_property used_in_implementation false $dcp
}
#read_xdc dont_touch.xdc
#set_property used_in_implementation false [get_files dont_touch.xdc]

set cached_ip [config_ip_cache -export -no_bom -use_project_ipc -dir . -new_name $IP_NAME -ip [get_ips $IP_NAME]]

if { $cached_ip eq {} } {
  # Only build design if it's not in the cache
  synth_design -top $IP_NAME -mode out_of_context

  #---------------------------------------------------------
  # Generate Checkpoint/Stub/Simulation Files For IP Cache
  #---------------------------------------------------------
  # disable binary constraint mode for IPCache checkpoints
  set_param constraints.enableBinaryConstraints false

  # put it in the cache now that it's built
  write_checkpoint -force -noxdef -rename_prefix [format "%s_" $IP_NAME] $IP_NAME.dcp
  set ipCachedFiles {}
  set file_postfixes [list "_stub.v" "_stub.vhdl" "_sim_netlist.v" "_sim_netlist.vhdl"]
  foreach postfix $file_postfixes {
    write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ $IP_NAME$postfix
    lappend ipCachedFiles $IP_NAME$postfix
  }
  config_ip_cache -add -dcp $IP_NAME.dcp -move_files $ipCachedFiles -use_project_ipc -ip [get_ips $IP_NAME]

  rename_ref -prefix_all [format "%s_" $IP_NAME]

  # disable binary constraint mode for synth run checkpoints
  set_param constraints.enableBinaryConstraints false
  write_checkpoint -force -noxdef $IP_NAME.dcp

  set r_name [format "%s_synth_report_utilization_0"  $IP_NAME]
  set r_command [format "report_utilization -file %s_utilization_synth.rpt -pb %s_utilization_synth.pb" $IP_NAME $IP_NAME]
  create_report $r_name $r_command
}
