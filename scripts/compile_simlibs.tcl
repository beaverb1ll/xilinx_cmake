set SCRIPT_DIR [file dirname $argv0]
set OUTPUT_DIR [lindex $argv 0]

source $SCRIPT_DIR/part_and_board.tcl

set PART_FAMILY [get_property FAMILY [get_property PART [current_project]]]

compile_simlib -force -simulator vcs_mx -family $PART_FAMILY -directory $OUTPUT_DIR
