set SCRIPT_DIR [file dirname $argv0]
set BD_NAME [lindex $argv 0]
set OUTPUT_DIR [pwd]

source $SCRIPT_DIR/part_and_board.tcl

create_bd_design -dir $OUTPUT_DIR $BD_NAME
